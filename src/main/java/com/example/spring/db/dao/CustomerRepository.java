package com.example.spring.db.dao;

import com.example.spring.db.Customer;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by MiguelGomes on 7/23/16.
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {

    Customer findByFirstName(String firstName);

    List<Customer> findByLastName(String lastName);

}
