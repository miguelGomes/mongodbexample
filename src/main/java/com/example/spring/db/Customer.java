package com.example.spring.db;

import com.google.common.base.MoreObjects;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by MiguelGomes on 7/23/16.
 */
/**
 * MongoDB stores data in collections. Spring Data MongoDB will map the class Customer into a
 * collection called customer. If you want to change the name of the collection,
 * you can use Spring Data MongoDB’s @Document annotation on the class.
 */
@Document(collection = "customer")
public class Customer {

    /**
     *  fits the standard name for a MongoDB id so it doesn’t require any special
     *  annotation to tag it for Spring Data MongoDB.
     */
    @Id
    private String id;

    private String firstName;

    private String lastName;

    private int age;

    public Customer(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("age", age)
                .toString();
    }
}
