package com.example.spring;

import com.example.spring.db.Customer;
import com.example.spring.db.dao.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by MiguelGomes on 7/23/16.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private CustomerRepository customerRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {


        customerRepository.deleteAll();

        customerRepository.save(new Customer("Miguel", "Gomes", 20));
        customerRepository.save(new Customer("Joao", "Gomes", 20));

        //Get all
        customerRepository.findAll()
                .stream()
                .forEach(System.out::println);
    }
}
